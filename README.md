# Introduction to Open Source GIS: Using QGIS and PyQGIS

1. Dowload and Install [QGIS](https://qgis.org/en/site/forusers/download.html)
1. Download [Workshop Data](https://gitlab.com/enockseth/introduction-to-fossgis-qgis-and-pyqgis/-/raw/master/data/osm_nima_ghana.gpkg) 
1. [Session 1 - July 18, 2020](./introduction.md)
1. [Session 2 - July 25,  2020](./pypy.md)

# References
1. ...
1. ..